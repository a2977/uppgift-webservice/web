package com.example.client;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class ClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }

    @Bean
    public CommandLineRunner smartGroups(GroupRepository repository){
     return args -> {
         repository.save(new Groups("astro", List.of("jakob", "adam")));
         repository.save(new Groups("aporna", List.of("anders", "erik")));
         repository.save(new Groups("kalleanka fans", List.of("nils", "per")));

     };
    }


}
