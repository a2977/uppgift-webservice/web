package com.example.client;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/groups")
@AllArgsConstructor
@CrossOrigin(allowedHeaders = "*", origins = "*")
public class GroupController {

    GroupRepository repository;
    GroupService groupService;

    private static final String CODE = "123";


    @GetMapping("/all")
    public List<Groups> GetAllGroups(@RequestHeader(value="code",required = false) String code){
        if (!CODE.equals(code)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        return repository.findAll();
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<Groups> getById(@PathVariable(value= "id")String id,@RequestHeader(value="code",required = false) String code ) throws Exception {
        if (!CODE.equals(code)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        Groups person = repository.findById(id).orElseThrow(() -> new Exception("group id: " + id +" not found!  "  ));
        return ResponseEntity.ok().body(person);
    }

    @PostMapping("/create")
    public Groups createGroup(@Valid @RequestBody Groups groupDetails,@RequestHeader(value="code",required = false) String code){
        if (!CODE.equals(code)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        groupDetails.setId(UUID.randomUUID().toString());
        groupDetails.setGroupMembers(List.of());
        return repository.save(groupDetails);
    }


    @PostMapping("/addMember/{memberId}/{groupId}")
    public String addMember(@PathVariable(value = "memberId") String memberId, @PathVariable(value = "groupId") String groupId,@RequestHeader(value="code",required = false) String code) throws Exception {
        if (!CODE.equals(code)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        Groups groups = repository.findById(groupId).orElseThrow(() -> new Exception("group id: " + groupId + " not found!  "));
        groups.groupMembers.add(memberId);
        repository.save(groups);
        return "successfully added members";
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<Groups > updateGroup(@PathVariable( value="id") String id, @Valid @RequestBody Groups groupDetails,@RequestHeader(value="code",required = false) String code) throws Exception {
        if (!CODE.equals(code)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        return groupService.updateGroup(id, groupDetails);
    }


    @DeleteMapping("/delete/{id}")
    public String deleteGroup(@PathVariable(value ="id") String id,@RequestHeader(value="code",required = false) String code) throws Exception {
        if (!CODE.equals(code)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        Groups group = repository.findById(id).orElseThrow( () ->  new Exception("group with id: " + id +" was not found " ));
        repository.delete(group);
        return "Delete successful!";
    }

    @DeleteMapping("/deleteMember/{memberId}/{groupId}")
    public String deleteMember(@PathVariable(value ="memberId") String memberId, @PathVariable(value ="groupId") String groupId,@RequestHeader(value="code",required = false) String code) throws Exception{
        if (!CODE.equals(code)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        Groups groups = repository.findById(groupId).orElseThrow(() -> new Exception("group id: " + groupId + " not found!  "));
        groups.groupMembers.remove(memberId);
        repository.save(groups);
        return "successfuly deleted";
    }
    @DeleteMapping("/deleteMember/{memberId}")
    public String deletePersonFromGroup(@PathVariable(value ="memberId") String memberId,@RequestHeader(value="code",required = false) String code) throws Exception{
        if (!CODE.equals(code)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
        groupService.deleteMembersFromAllGroups(memberId);
        return "successfully deleted person "+memberId+" from groups";
    }
}
