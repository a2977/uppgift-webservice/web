package com.example.client;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupService {
    private static final String CODE = "123";

    GroupRepository groupRepository;

    public GroupService(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    public String deleteMembersFromAllGroups(String memberId){
        List<Groups> groupsList = groupRepository.findAll().stream().filter(groups -> groups.groupMembers.contains(memberId)).collect(Collectors.toList());
        groupsList.forEach(groups -> groups.deleteMember(memberId));
        return "delete successful";
    }

    public ResponseEntity<Groups> updateGroup(String id, Groups groupDetails) throws Exception {
        Groups group = groupRepository.findById(id).orElseThrow( () ->  new Exception("group with id: " + id +" was not found " ));
        group.setGroupName(groupDetails.getGroupName());
        group.setGroupMembers(groupDetails.getGroupMembers());
        final Groups updatePerson = groupRepository.save(group);
        return ResponseEntity.ok(updatePerson);
    }
}
