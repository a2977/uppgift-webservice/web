package com.example.client;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity(name="Groups")
public class Groups {
    @Id
    String id;
    String groupName;
    @ElementCollection
    List<String> groupMembers;

    public Groups( String groupName, List<String> groupMembers) {
        this.id = UUID.randomUUID().toString();
        this.groupName = groupName;
        this.groupMembers = groupMembers;
    }

    void deleteMember(String id){
        for(String memberId : groupMembers){
            if(memberId.equals(id)){
                groupMembers.remove(id);
            }
        }
    }
}
